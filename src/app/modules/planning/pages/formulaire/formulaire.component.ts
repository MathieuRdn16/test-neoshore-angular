import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Mission } from '../../models/mission';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.scss']
})
export class FormulaireComponent implements OnInit {

  @Input()
  id?:string;
  @Input()
  titre?:string;
  @Input()
  description?:string;
  @Input()
  couleur?:string;
  @Input()
  debut?:string;
  @Input()
  fin?:string;
  error=0;
  @Input()
  libelle:string="Ajouter";
  constructor(
    private activeModal: NgbActiveModal,
  ) { }

  ngOnInit(): void {
    if(this.debut!=undefined)
      this.debut=this.dateFormat(new Date(this.debut));
    if(this.fin!=undefined)
      this.fin=this.dateFormat(new Date(this.fin));
  }


  dateFormat(date: Date){
    if(date==null) return "";
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');

    return  `${year}-${month}-${day}T${hours}:${minutes}`;
  }
  delete(){
    this.activeModal.close(-1);
  }
  retour(){
    this.activeModal.close(1);
  }

  async missionAddOrModif(){
    await this.verification();
    if(this.error==1) return;
    let db:Date|undefined ;
    let fn:Date|undefined ;
    if(this.debut!=null) db=new Date(this.debut);
    if(this.fin!=null) fn=new Date(this.fin);
    const mission =new Mission(this.id,this.titre,this.description,this.couleur,db,fn);
    console.log(mission)
    this.activeModal.close(mission);
  }

  isBlank(str: string): boolean {
    return !str || str.trim().length === 0;
  }

  async verification() :Promise<any> {
    if(this.titre==undefined || this.isBlank(this.titre)) return this.error=1;
    if(this.description==undefined || this.isBlank(this.description)) return this.error=1;
    if(this.couleur==undefined || this.isBlank(this.couleur)) return this.error=1;
    if(this.debut==undefined || this.isBlank(this.debut)) return this.error=1;
    if(this.fin==undefined || this.isBlank(this.fin)) return this.error=1;
  }

}

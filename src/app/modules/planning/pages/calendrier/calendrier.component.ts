import { Component, OnInit } from '@angular/core';
import { CalendarOptions } from '@fullcalendar/core';
import timeGridPlugin from '@fullcalendar/timegrid';
import dayGridPlugin from '@fullcalendar/daygrid'
import frLocale from '@fullcalendar/core/locales/fr';
import { DataService } from '../../services/data.service';
import { Personne } from '../../models/personne';
import { FormulaireComponent } from '../formulaire/formulaire.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {  Mission } from '../../models/mission';
import { EventApi } from '@fullcalendar/core';


@Component({
  selector: 'app-calendrier',
  templateUrl: './calendrier.component.html',
  styleUrls: ['./calendrier.component.scss']
})
export class CalendrierComponent implements OnInit {
  calendarOptions: CalendarOptions = {
    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'timeGridDay,timeGridWeek,dayGridMonth'
    },
    initialView: 'timeGridWeek',

    plugins: [dayGridPlugin,timeGridPlugin],
    locale: frLocale,
    eventClick: this.AjoutEtModification.bind(this),
    eventMouseEnter:this.voirDescription.bind(this),
    eventMouseLeave:this.mouseLeave.bind(this)

  };

  search?:string;
  agentId:any;
  agent?:Personne;
  personnes:Personne[]=[];
  description?:string;
  viewDescription:boolean=false;
  mouseX: number = 0;
  mouseY: number = 0;
  constructor(
    private dataService: DataService,
    private modal:NgbModal,
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.dataService.getData('data').subscribe(data => {
      this.personnes = data.personnes;
    });

  }

  changeAgent(){
    if(this.agentId==undefined) return;
    const index = this.personnes.findIndex(obj => obj.id=== this.agentId);
    if (index !== -1) {
      this.agent=this.personnes[index];
      this.listCalendar();
    }
  }

  listCalendar(){
    if(this.agent==undefined) return;
    const liste:any[]=[];
    for (let ms of this.agent.missions) {
      liste.push({
        title: ms.titre,
        start: ms.debut,
        end:ms.fin,
        backgroundColor: ms.couleur,
        id:ms.id,
        mission:ms
      });
    }
    this.calendarOptions.events=liste;
  }

  viewDescri(mission:any){
    if(this.agent==undefined) return;
    const liste:any[]=[];
    for (let ms of this.agent.missions) {
      if(mission.id==ms.id){
        liste.push({
          title: ms.descri,
          start: ms.debut,
          end:ms.fin,
          backgroundColor: ms.couleur,
          id:ms.id,
          mission:ms
        });
      }
      else{
          liste.push({
            title: ms.titre,
            start: ms.debut,
            end:ms.fin,
            backgroundColor: ms.couleur,
            id:ms.id,
            mission:ms
          });
      }

    }
    this.calendarOptions.events=liste;
  }


  modifier(){
    console.log(this.agent);
  }

  changeOrSupprMission(mission:Mission,modif:number){
    if(this.agent==undefined) return;
    const index = this.agent.missions.findIndex(obj => obj.id=== mission.id);
    if (index !== -1) {
      this.agent.missions.splice(index,1);
      if(modif==1) this.agent.missions.push(mission);
      this.listCalendar();
    }
  }





  addMission(mission:Mission){
    if(this.agent==undefined) return;
    mission.id=this.agent.id+new Date().toISOString();
    this.agent.missions.push(mission);
      this.listCalendar();
  }

  AjoutEtModification(arg:any){
    const modalRef = this.modal.open(FormulaireComponent,{size:'xl',centered:true,scrollable:true,backdrop:'static'});
      let mission:any;
      if(arg!=undefined) {
        mission=arg.event.extendedProps.mission;
        modalRef.componentInstance.id = mission.id;
        modalRef.componentInstance.titre = mission.titre;
        modalRef.componentInstance.description = mission.descri;
        modalRef.componentInstance.couleur = mission.couleur;
        modalRef.componentInstance.debut = mission.debut;
        modalRef.componentInstance.fin = mission.fin;
        modalRef.componentInstance.libelle = "Modifier";
      }
      modalRef.result.then((result) => {
        if(result==1) return;
        else if(result==-1&&mission!=undefined) this.changeOrSupprMission(mission,0);
        else if(result.id==undefined ) this.addMission(result);
        else if(result.id!=undefined ) this.changeOrSupprMission(result,1);
      }).catch((reason) => {

      });
  }

  voirDescription(arg:any) {
    const event = arg.event;
    const mission= event.extendedProps.mission;
    this.viewDescription=true;
    this.viewDescri(mission);

  }

  mouseLeave() {
    this.description=undefined;
    this.viewDescription=false;
    this.listCalendar();
  }
}

export class Mission {
  id?:string;
  titre?:string;
  descri?:string;
  couleur?:string;
  debut?:Date;
  fin?:Date;

  constructor(id?:string,titre?:string,descri?:string,couleur?:string,debut?:Date,fin?:Date) {
    this.id = id;
    this.titre=titre;
    this.descri=descri;
    this.couleur=couleur;
    this.debut=debut;
    this.fin=fin;
  }
}

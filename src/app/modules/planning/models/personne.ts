import { Mission } from "./mission";

export class Personne {
  id?:string;
  nom?:string;
  prenoms?:string;
  photo?:string;
  missions:Mission[]=[];

  constructor(id:string,nom:string,prenoms:string,photo:string,missions:Mission[]){
    this.id=id;
    this.nom = nom;
    this.prenoms = prenoms;
    this.photo = photo;
    this.missions = missions;
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlanningComponent } from './planning.component';
import { CalendrierComponent } from './pages/calendrier/calendrier.component';

const routes: Routes = [
  {
    path: '',

    component: PlanningComponent,
    children: [
      {
        path: '',
        redirectTo: 'calendrier',
        pathMatch: 'full',
      },
      {
        path: 'calendrier', // child route path
        component: CalendrierComponent, // child route component that the router renders
      },

    ],
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanningRoutingModule { }

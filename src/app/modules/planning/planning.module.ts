import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlanningRoutingModule } from './planning-routing.module';
import { PlanningComponent } from './planning.component';
import { CalendrierComponent } from './pages/calendrier/calendrier.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FormulaireComponent } from './pages/formulaire/formulaire.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    PlanningComponent,
    CalendrierComponent,
    FormulaireComponent,
  ],
  imports: [
    CommonModule,
    FullCalendarModule,
    PlanningRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbModule
  ]
})
export class PlanningModule { }

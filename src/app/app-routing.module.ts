import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'planning',
    pathMatch: 'full',
  },

  {
    path: 'planning',
    loadChildren: () =>
      import('./modules/planning/planning.module').then((m) => m.PlanningModule),
  },

  {
    path: '**',
    redirectTo: 'planning',
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
